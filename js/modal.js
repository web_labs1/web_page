// const dialog = document.querySelector("dialog");
// const cancelButton = document.getElementById("modal_cancel");

// cancelButton.addEventListener("click", () => {
//     dialog.close();
//   });

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

var send_form = document.getElementById("send_form");
var phone_number = document.getElementById("phone_number");
var name_field = document.getElementById("name_field");
var surname_field = document.getElementById("surname_field");
var message_field = document.getElementById("message_field");
var email_field = document.getElementById("email_field");

function save_to_localstorage() {
  localStorage.setItem("name_field", name_field.value)
  localStorage.setItem("surname_field", surname_field.value)
  localStorage.setItem("email_field", email_field.value)
  localStorage.setItem("phone_number", phone_number.value)
  localStorage.setItem("message_field", message_field.value)
}

function get_from_localstorage() {
  name_field.value = localStorage.getItem("name_field")
  surname_field.value = localStorage.getItem("surname_field")
  email_field.value = localStorage.getItem("email_field")
  phone_number.value = localStorage.getItem("phone_number")
  message_field.value = localStorage.getItem("message_field")
}

function set_cookie(key, value) {
  document.cookie = key+"="+value
}

function get_cookie(name) {
  const regex = new RegExp(`(^| )${name}=([^;]+)`)
  const match = document.cookie.match(regex)
  if (match) {
    return match[2]
  }
}

// When the user clicks on the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
  try {
    get_from_localstorage()
  } catch {
    console.log("no values in local storage")
  }
}

send_form.onclick = function() {
  console.log("click")
  var vname_field = validate_field(/^[a-zA-Z]+$/, "name_field")
  var vsurname_field = validate_field(/^[a-zA-Z]+$/, "surname_field")
  var vemail_field = validate_field(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, "email_field")
  var vphone_number = validate_field(/^\+\d\(?\d{3}\)?\d{2}\-?\d{2}\-?\d{3}$/, "phone_number")
  var vmessage_field = validate_field(/.+/, "message_field")
  var c_feedbk_form = get_cookie("feedbk_form")
  var c_name_field = get_cookie("name_field")
  var c_surname_field = get_cookie("surname_field")
  if (c_feedbk_form == "true") {
      alert(c_name_field+" "+c_surname_field+", your request is being processed")
  } else {
    if (vname_field && vsurname_field && vemail_field && vphone_number && vmessage_field) {
      set_cookie("feedbk_form", "true")
      set_cookie("name_field", name_field.value)
      set_cookie("surname_field", surname_field.value)
      alert(name_field.value+" "+surname_field.value+", thanks for feedback")
      modal.style.display = "none";
    }
  } 
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
  if (phone_number !== "" || name_field !== "" || surname_field !== "" || message_field !== "" || email_field !== ""){
    save_to_localstorage()
  }
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
    save_to_localstorage()
  }
}

// on click send button
email_field.onchange = function() {
  if (validate_field(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/, "email_field")) {
  console.log("okay")
  } else {
  console.log("no ok")
  }
}

// on change phone field
phone_number.onchange = function() {
  if (validate_field(/^\+\d\(?\d{3}\)?\d{2}\-?\d{2}\-?\d{3}$/, "phone_number")) {
    console.log("phone looks good")
  } else {
    console.log("phone looks no good")
  }
}

name_field.onchange = function() {
  if (validate_field(/^[a-zA-Z]+$/, "name_field")) {
    console.log("name looks good")
  } else {
    console.log("name looks no good")
  }
}

surname_field.onchange = function() {
  if (validate_field(/^[a-zA-Z]+$/, "surname_field")) {
    console.log("surname looks good")
  } else {
    console.log("surname looks no good")
  }
}

message_field.onchange = function() {
  if (validate_field(/.+/, "message_field")) {
    console.log("message_field looks good")
  } else {
    console.log("message_field looks no good")
  }
}

function validate_field(regex, id) {
  var text = document.getElementById(id);
  if (text.value.match(regex)) {
    text.style.borderColor = "black";
    return true
  } else {
    text.style.borderColor = "red";
    return false
  }
}


